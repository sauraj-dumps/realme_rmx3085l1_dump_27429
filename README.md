## sys_mssi_64_cn_armv82-user 12 SP1A.210812.016 1652266053889 release-keys
- Manufacturer: realme
- Platform: mt6785
- Codename: RMX3085L1
- Brand: realme
- Flavor: lineage_nashc-userdebug
- Release Version: 12
- Id: SP2A.220505.002
- Incremental: eng.ben.20220604.082043
- Tags: dev-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/RMX3085/RMX3085L1:12/SP1A.210812.016/R.202205111917:user/release-keys
- OTA version: 
- Branch: sys_mssi_64_cn_armv82-user-12-SP1A.210812.016-1652266053889-release-keys
- Repo: realme_rmx3085l1_dump_27429


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
